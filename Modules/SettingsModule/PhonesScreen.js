import React from 'react';
import { View, Text, Alert, TextInput, StyleSheet, KeyboardAvoidingView, FlatList, Modal } from 'react-native';
import SettingsStorage from '../../Database/SettingsStorage';
import SMSStorage from '../../Database/SMSStorage';
import SmsOperations from '../../SMS/SmsOperations';
import Icon from 'react-native-vector-icons/Fontisto';
import { ListItem } from "react-native-elements";
import MyButton from '../../MyButton';
import { ScaledSheet } from 'react-native-size-matters';

//TODO zdejmowanie blokady za 1 szym razem, gdy jest wymagana zmiana ustawień przez wysłanie sms-a

export default class PhonesScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sms: props.navigation.state.params.sms,
            isVisible: false,
            phonesList: [],
            phoneNumber: '',
            idToAdd: null
        }

        this.phoneNumberIsValid = false;
    }

    async componentDidMount() {
        SettingsStorage.getPhoneNumbers().then(result => this.setState({ phonesList: result }));
    }

    async componentWillUnmount() {
        SettingsStorage.setPhoneNumbers(this.state.phonesList);
        console.log("Wychodzę z ustawień!");
        if (await SettingsStorage.isChangedAlarmStatus()) {
            SMSStorage.getAlarmLastStatus().then(result => {
                if (result === null || !result.status) {
                    SmsOperations.sendArmingRequest();
                    SettingsStorage.unsetChangedAlarmStatus();
                }
            });
        }
    }

    static navigationOptions = ({ navigation }) => ({
        title: 'Zaufane numery telefonów',
    });

    async removeLock() {
        if (!await SettingsStorage.isChangedAlarmStatus()) {
            let lastStatus = await SMSStorage.getAlarmLastStatus();
            if (lastStatus === null || lastStatus.status === null || lastStatus.status) {
                SmsOperations.sendDisarmingRequest();
                await SettingsStorage.setChangedAlarmStatus();
            }
        }
    }

    onChangedPhoneNumber(text) {
        this.setState({ phoneNumber: text });
    }

    addPhoneNumber() {
        let id = this.state.idToAdd;
        let phoneNumber = this.state.phoneNumber;
        this.validatePhoneNumber();
        if (this.phoneNumberIsValid) {
            let phones = this.state.phonesList;
            phones[id] = { id: id, phone: this.state.phoneNumber };
            this.setState({ phonesList: phones, idToAdd: null, phoneNumber: '', isVisible: false });

            SettingsStorage.setPhoneNumbers(this.state.phonesList);
            if (this.state.sms) {
                this.removeLock();
                setTimeout(() => {
                    SmsOperations.sendAddPhoneNumber(id + 1, phoneNumber.split('+')[1]);;
                }, 2000);
            }
        }

    }

    removePhoneNumber(id) {
        let phones = this.state.phonesList;
        phones[id] = { id: id, phone: '' };;
        this.setState({ phonesList: phones });

        SettingsStorage.setPhoneNumbers(this.state.phonesList);

        if (this.state.sms) {
            this.removeLock();
            setTimeout(() => {
                SmsOperations.sendRemovePhoneNumber(id + 1);
            }, 2000);
            
        }
    }

    validateNumber(text) {
        let newText = '';
        let numbers = '0123456789+';

        for (let i = 0; i < text.length; i++) {
            if (numbers.indexOf(text[i]) > -1) {
                newText = newText + text[i];
            }
            else {
                return false;
            }
        }

        return true;
    }

    validatePhoneNumber() {
        let status = this.validateNumber(this.state.phoneNumber);

        if (this.state.phoneNumber.startsWith('+') && status && this.state.phoneNumber.length == 12)
            this.phoneNumberIsValid = true;
        else {
            this.setState({ phoneNumber: '' });
            this.phoneNumberIsValid = false;
        }
    }

    render() {
        return (
            <>
                <Modal
                    style={{ marginBottom: 0, padding: 0 }}
                    animationType='fade'
                    transparent={true}
                    visible={this.state.isVisible}
                >
                    <KeyboardAvoidingView style={styles.modal} behavior="height" enabled>
                        <View style={{ flex: 1, margin: '5%', padding: 0 }}>
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: '5%' }}>
                                <Text style={styles.modalTitle}>Dodawanie numeru telefonu</Text>
                            </View>
                            <View style={{ flex: 3, justifyContent: 'center', alignItems: 'stretch', alignSelf: 'stretch', alignContent: 'stretch', margin: 0 }}>
                                <Text style={{ marginVertical: '2%' }}>Numer telefonu: </Text>
                                <TextInput
                                    placeholder='+48123456789'
                                    placeholderTextColor={placeholderColor}
                                    keyboardType='phone-pad'
                                    onChangeText={(text) => this.onChangedPhoneNumber(text)}
                                    value={this.state.phoneNumber}
                                    maxLength={12}
                                    style={styles.textInput}
                                />
                            </View>
                            <View style={{ flex: 2, marginTop: '5%' }}>
                                <MyButton title='Zatwierdź' color={iconColor} onPress={() => { this.addPhoneNumber() }} />
                                <MyButton style={{ marginTop: '2%' }} title='Anuluj' color={"#ff2828"} onPress={() => {
                                    this.setState({
                                        isVisible: false,
                                        phoneNumber: '',
                                        idToAdd: null
                                    })
                                }} />
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </Modal>

                <FlatList
                    data={this.state.phonesList}
                    renderItem={({ item }) => (
                        <ListItem onLongPress={() => {
                            if (item.phone !== '') {
                                Alert.alert("Usunięcie numeru telefonu", "Czy na pewno chcesz usunąć numer telefonu " + item.phone + "?",
                                    [
                                        {
                                            text: 'Anuluj',
                                            style: 'cancel',
                                        },
                                        { text: 'Tak', onPress: () => this.removePhoneNumber(item.id) },
                                    ],
                                )
                            }
                        }}
                            onPress={() => {
                                if (item.phone === '') {
                                    this.setState({ isVisible: true, idToAdd: item.id })
                                }
                            }}
                            title={item.id + 1 + ' numer telefonu'}
                            titleStyle={{}}
                            subtitle={item.phone}
                            subtitleStyle={{ color: "#4F8EF7" }}
                            bottomDivider
                            leftIcon={<Icon name={'phone'} size={25} color={"#4F8EF7"} />}
                        />
                    )}
                    keyExtractor={item => item.id}
                />
            </>)
    }
}

const iconColor = "#4F8EF7";
const placeholderColor = "#b8b8b8";

const styles = ScaledSheet.create({
    modal: {
        marginBottom: '0%',
        marginTop: '20%',
        marginHorizontal: '5%',
        flex: 0.8,
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.46,
        shadowRadius: 9.51,
        elevation: 15,
    },

    textInput: {
        borderStyle: "solid",
        borderColor: iconColor,
        borderWidth: 1,
        borderRadius: 5,
        textAlign: 'center',
        fontSize: "20@s"
    },

    modalTitle: {
        flex: 1,
        fontWeight: 'bold',
        marginVertical: '1%',
        fontSize: "30@s"
    },
});