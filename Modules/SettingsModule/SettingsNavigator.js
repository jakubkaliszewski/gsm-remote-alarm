import { createStackNavigator } from 'react-navigation-stack';
import SettingsScreen from './SettingsScreen';
import PhonesScreen from './PhonesScreen';
import ClockScreen from './ClockScreen';
import ExistingScreen from './ExistingScreen';

export default SettingsNavigator = createStackNavigator({
    Settings: {
        screen: SettingsScreen,
    },
    PhonesScreen: {
        screen: PhonesScreen,
    },
    ClockScreen:{
        screen: ClockScreen
    },
    ExistingScreen: {
        screen: ExistingScreen
    }

}, {
    initialRouteName: 'Settings',
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: 'white',
        },
        headerTintColor: 'black',
        headerTitleStyle: {
            fontWeight: 'bold',
            textAlign: 'center',
            flex: 1,
        },
    },
});