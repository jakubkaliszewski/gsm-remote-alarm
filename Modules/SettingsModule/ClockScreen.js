import React from 'react';
import { FlatList, Picker, View, Text, Modal, StyleSheet } from 'react-native';
import SettingsStorage from '../../Database/SettingsStorage';
import SMSStorage from '../../Database/SMSStorage';
import SmsOperations from '../../SMS/SmsOperations';
import IconFontiso from 'react-native-vector-icons/Fontisto';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import { ListItem } from "react-native-elements";
import DateTimePicker from '@react-native-community/datetimepicker';
import MyButton from '../../MyButton';
import { ScaledSheet } from 'react-native-size-matters';


//TODO zdejmowanie blokady za 1 szym razem, gdy jest wymagana zmiana ustawień przez wysłanie sms-a

export default class ClockScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sms: props.navigation.state.params.sms,
            showArmingClockSettings: false,
            showDisarmingClockSettings: false,
            showClockSettings: false,
            showArmingDelayTime: false,
            options: this.setOptions(props.navigation.state.params.sms),
            delayOptions: [
                { key: '1', value: '10 sekund' },
                { key: '2', value: '20 sekund' },
                { key: '3', value: '30 sekund' },
                { key: '4', value: '40 sekund' },
                { key: '5', value: '50 sekund' },
                { key: '6', value: '60 sekund' },
                { key: '7', value: '70 sekund' },
                { key: '8', value: '80 sekund' },
                { key: '9', value: '90 sekund' },
            ],
        }
    }

    setOptions(withSMS) {
        if (withSMS) {
            return [
                { id: '1', name: "Ustaw zegar alarmu", subname: "", iconSet: 'fontisto', icon: "clock", func: () => { this.setClock() } }, //dać referencję na funkcję
                { id: '2', name: "Czasowe uzbrajanie", subname: "", iconSet: 'fontisto', icon: "bell", func: () => { this.setArmingClock() } },
                { id: '3', name: "Czasowe rozbrajanie", subname: "", iconSet: 'fontisto', icon: "bell-alt", func: () => { this.setDisarmingClock() } },
                { id: '4', name: "Opóźnienie uzbrojenia", subname: "", iconSet: 'font-awesome', icon: "hourglass", func: () => { this.setArmingDelayTime() } },
                { id: '5', name: "Usuń czasowe uzbrajanie", subname: "", iconSet: 'fontisto', icon: "trash", func: () => { this.sendRemoveArmingClock() } },
                { id: '6', name: "Usuń czasowe rozbrajanie", subname: "", iconSet: 'fontisto', icon: "trash", func: () => { this.sendRemoveDisarmingClock() } },
                { id: '7', name: "Usuń opóźnienie uzbrojenia", subname: "", iconSet: 'fontisto', icon: "trash", func: () => { this.sendRemoveArmingDelayTime() } }
            ]
        } else {
            return [
                { id: '1', name: "Czasowe uzbrajanie", subname: "", iconSet: 'fontisto', icon: "bell", func: () => { this.setArmingClock() } },
                { id: '2', name: "Czasowe rozbrajanie", subname: "", iconSet: 'fontisto', icon: "bell-alt", func: () => { this.setDisarmingClock() } },
                { id: '3', name: "Opóźnienie uzbrojenia", subname: "", iconSet: 'font-awesome', icon: "hourglass", func: () => { this.setArmingDelayTime() } },
                { id: '4', name: "Usuń czasowe uzbrajanie", subname: "", iconSet: 'fontisto', icon: "trash", func: () => { this.sendRemoveArmingClock() } },
                { id: '5', name: "Usuń czasowe rozbrajanie", subname: "", iconSet: 'fontisto', icon: "trash", func: () => { this.sendRemoveDisarmingClock() } },
                { id: '6', name: "Usuń opóźnienie uzbrojenia", subname: "", iconSet: 'fontisto', icon: "trash", func: () => { this.sendRemoveArmingDelayTime() } }
            ]
        }
    }

    async componentDidMount() {
        this.getTimeRules();
    }

    async getTimeRules() {
        SettingsStorage.getArmingRule().then(result => {
            if (result !== null) {
                if(!this.state.sms){
                    let temp = this.state.options;
                    temp[0].subname = result;
                    this.setState({ options: temp });
                }else{
                    let temp = this.state.options;
                    temp[1].subname = result;
                    this.setState({ options: temp });
                }
            } else {
                if (!this.state.sms) {
                    let temp = this.state.options;
                    temp[0].subname = '';
                    this.setState({ options: temp });
                } else {
                    let temp = this.state.options;
                    temp[1].subname = '';
                    this.setState({ options: temp });
                }
            }
        });

        SettingsStorage.getDisarmingRule().then(result => {
            console.log('Disarming: ' + result);

            if (result !== null) {
                if (!this.state.sms) {
                    let temp = this.state.options;
                    temp[1].subname = result;
                    this.setState({ options: temp });
                } else {
                    let temp = this.state.options;
                    temp[2].subname = result;
                    this.setState({ options: temp });
                }
            } else {
                if (!this.state.sms) {
                    let temp = this.state.options;
                    temp[1].subname = '';
                    this.setState({ options: temp });
                } else {
                    let temp = this.state.options;
                    temp[2].subname = '';
                    this.setState({ options: temp });
                }
            }
        });

        SettingsStorage.getArmingDelayRule().then(result => {
            console.log('Delay arming: ' + result);

            if (result !== null) {
                if (!this.state.sms) {
                    let temp = this.state.options;
                    temp[2].subname = result;
                    this.setState({ options: temp });
                } else {
                    let temp = this.state.options;
                    temp[3].subname = result;
                    this.setState({ options: temp });
                }
            } else {
                if (!this.state.sms) {
                    let temp = this.state.options;
                    temp[2].subname = '';
                    this.setState({ options: temp });
                } else {
                    let temp = this.state.options;
                    temp[3].subname = '';
                    this.setState({ options: temp });
                }
            }
        });
    }

    async componentWillUnmount() {
        console.log("Wychodzę z ustawień! " + await SettingsStorage.isChangedAlarmStatus());
        if (await SettingsStorage.isChangedAlarmStatus()) {
            SMSStorage.getAlarmLastStatus().then(result => {
                if (result === null || !result.status) {
                    SmsOperations.sendArmingRequest();
                    SettingsStorage.unsetChangedAlarmStatus();
                }
            });
        }
    }

    static navigationOptions = ({ navigation }) => ({
        title: 'Ustawienia czasowe',
    });

    async removeLock() {
        if (!await SettingsStorage.isChangedAlarmStatus()) {
            let lastStatus = await SMSStorage.getAlarmLastStatus();
            if (lastStatus === null || lastStatus.status === null || lastStatus.status) {
                SmsOperations.sendDisarmingRequest();
                await SettingsStorage.setChangedAlarmStatus();
            }
        }
    }

    setClock() {
        this.setState({ showClockSettings: true });
    }

    setArmingClock() {
        this.setState({ showArmingClockSettings: true });
    }

    setDisarmingClock() {
        this.setState({ showDisarmingClockSettings: true });
    }

    setArmingDelayTime() {
        this.setState({ showArmingDelayTime: true });
    }


    sendClock = (event, date) => {
        this.setState({ showClockSettings: false });
        if (date !== undefined) {
            let hour = date.getHours();
            let minutes = date.getMinutes();

            if (this.state.sms) {
                this.removeLock();
                setTimeout(() => {
                    SmsOperations.sendSetClock(hour, minutes);
                }, 2000);
            }
        }
    }

    sendArmingClock = (event, date) => {
        this.setState({ showArmingClockSettings: false });
        if (date !== undefined) {
            let hour = date.getHours();
            let minutes = date.getMinutes();

            let minutesString = minutes.toString();
            let hourString = hour.toString();
            if (minutesString.length === 1) {
                minutesString = '0' + minutes;
            }
            if (hourString.length === 1) {
                hourString = '0' + hour;
            }

            if (this.state.sms) {
                this.removeLock();
                setTimeout(() => {
                    SmsOperations.sendArmingClock(hour, minutes);
                }, 2000);
            }

            SettingsStorage.setArmingRule(hourString + ':' + minutesString);
            this.getTimeRules();
        }
    }

    sendDisarmingClock = (event, date) => {
        this.setState({ showDisarmingClockSettings: false });
        if (date !== undefined) {
            let hour = date.getHours();
            let minutes = date.getMinutes();

            let minutesString = minutes.toString();
            let hourString = hour.toString();
            if (minutesString.length === 1) {
                minutesString = '0' + minutes;
            }
            if (hourString.length === 1) {
                hourString = '0' + hour;
            }

            if (this.state.sms) {
                this.removeLock();
                setTimeout(() => {
                    SmsOperations.sendDisarmingClock(hourString, minutesString);
                }, 2000); Overlay
            }

            SettingsStorage.setDisarmingRule(hourString + ':' + minutesString);
            this.getTimeRules();
        }
    }

    sendArmingDelayTime(seconds){
        this.setState({ showArmingDelayTime: false });
        if (seconds !== undefined) {
            if (this.state.sms) {
                this.removeLock();
                setTimeout(() => {
                    SmsOperations.sendArmingDelay(seconds + 1);
                }, 2000);
            }

            SettingsStorage.setArmingDelayRule(this.state.selectedDelay);
            this.getTimeRules();
        }
    }

    sendRemoveArmingClock = (event) => {
        if (this.state.sms) {
            this.removeLock();
            setTimeout(() => {
                SmsOperations.sendArmingClockOff();
            }, 2000);
        }

        SettingsStorage.removeArmingRule();
        this.getTimeRules();
    }

    sendRemoveDisarmingClock = (event) => {
        if (this.state.sms) {
            this.removeLock();
            SmsOperations.sendDisarmingClockOff();
        }

        SettingsStorage.removeDisarmingRule();
        this.getTimeRules();
    }

    sendRemoveArmingDelayTime = (event) => {
        if (this.state.sms) {
            this.removeLock();
            SmsOperations.sendArmingDelayOff();
        }

        SettingsStorage.removeArmingDelayRule();
        this.getTimeRules();
    }

    renderIcon = (iconSet, iconName) => {
        if (iconSet == "fontisto") {
            return (
                <IconFontiso name={iconName} size={25} color={"#4F8EF7"} />
            );
        }
        if (iconSet == "font-awesome") {
            return (
                <IconAwesome name={iconName} size={25} color={"#4F8EF7"} />
            );
        }
    }

    render() {

        let delayItems = this.state.delayOptions.map((s, i) => {
            return <Picker.Item key={i} value={s.value} label={s.value} />
        });

        return (
            <>
                {this.state.showClockSettings && <DateTimePicker value={new Date()}
                    mode='time'
                    is24Hour={true}
                    display="default"
                    onChange={this.sendClock} />
                }
                {this.state.showArmingClockSettings && <DateTimePicker value={new Date()}
                    mode='time'
                    is24Hour={true}
                    display="default"
                    onChange={this.sendArmingClock} />
                }
                {this.state.showDisarmingClockSettings && <DateTimePicker value={new Date()}
                    mode='time'
                    is24Hour={true}
                    display="default"
                    onChange={this.sendDisarmingClock} />
                }
                {
                    <Modal
                        style={{ marginBottom: 0, padding: 0 }}
                        animationType='fade'
                        transparent={true}
                        visible={this.state.showArmingDelayTime}
                    >
                        <View style={styles.modal} behavior="height" enabled>
                            <View style={{ flex: 1, margin: '5%', padding: 0 }}>
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: '5%', alignContent: 'center' }}>
                                    <Text style={styles.modalTitle}>Opóźnienia uzbrojenia</Text>
                                </View>
                                <View style={{ flex: 2, justifyContent: 'center', alignItems: 'stretch', alignSelf: 'stretch', alignContent: 'stretch', margin: 0 }}>
                                    <Text style={{ marginVertical: '1%' }}>Opóźnienie uzbrojenia [s]: </Text>
                                    <Picker
                                        selectedValue={this.state.selectedDelay}
                                        style={{
                                            width: "100%", alignSelf: 'stretch',
                                            justifyContent: 'center'
                                        }}
                                        mode='dropdown'
                                        onValueChange={(itemValue, itemIndex) =>
                                            this.setState({ selectedDelay: itemValue, selectedDelayIndex: itemIndex })
                                        }>
                                        {delayItems}
                                    </Picker>
                                </View>
                                <View style={{ flex: 2, marginTop: '5%' }}>
                                    <MyButton title='Zatwierdź' color={iconColor} onPress={() => { this.sendArmingDelayTime(this.state.selectedDelayIndex)}} />
                                    <MyButton style={{ marginTop: '2%' }} title='Anuluj' color={"#ff2828"} onPress={() => {
                                        this.setState({
                                            showArmingDelayTime: false
                                        })
                                    }} />
                                </View>
                            </View>
                        </View>
                    </Modal>
                }

                <FlatList
                    data={this.state.options}
                    renderItem={({ item }) => (
                        <ListItem onPress={() => item.func()}
                            title={item.name}
                            titleStyle={{}}
                            subtitle={item.subname}
                            subtitleStyle={{ color: "#4F8EF7" }}
                            bottomDivider
                            leftIcon={this.renderIcon(item.iconSet, item.icon)}
                        />
                    )}
                    keyExtractor={item => item.id}
                />
            </>)
    }
}

const iconColor = "#4F8EF7";

const styles = ScaledSheet.create({
    modal: {
        marginBottom: '0%',
        marginTop: '20%',
        marginHorizontal: '5%',
        flex: 0.8,
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.46,
        shadowRadius: 9.51,
        elevation: 15,
    },
    modalTitle:{
        flex: 1, 
        fontWeight: 'bold', 
        marginVertical: '1%',
        fontSize: "25@s"
    },

    textInput: {
        borderStyle: "solid",
        borderColor: iconColor,
        borderWidth: 1,
        borderRadius: 5,
        textAlign: 'center',
        fontSize: "20@s"
    }
});