import React from 'react';
import { NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native';
import { ListItem } from "react-native-elements";
import Icon from 'react-native-vector-icons/Fontisto';

export default class SettingsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.options = [
            { id: '1', name: "Zaufane numery telefonów", icon: "phone", screen: "PhonesScreen", func: (screen) => { this.navigate(screen) } }, //dać referencję na funkcję
            { id: '2', name: "Ustawienia zegara", icon: "clock", screen: "ClockScreen", func: (screen) => { this.navigate(screen) } },
            { id: '3', name: "Ustaw istniejącą konfigurację", icon: "download", screen: "ExistingScreen", func: (screen) => { this.navigate(screen) } }, // sklonować  numery telefonów, uzbrajanie / rozbrajanie
        ];
    }

    static navigationOptions = ({ navigation }) => ({
        title: 'Ustawienia alarmu',
    });

    navigate(screen) {
        let navigationAction = NavigationActions.navigate({
            routeName: screen,
            params: {sms: true}
        });

        this.props.navigation.dispatch(navigationAction);
    }

    async componentWillUnmount() {
        console.log("Wychodzę z ustawień!");
        if (await SettingsStorage.isChangedAlarmStatus()) {
            SMSStorage.getAlarmLastStatus().then(result => {
                if (result === null || !result.status) {
                    SmsOperations.sendArmingRequest();
                    SettingsStorage.unsetChangedAlarmStatus();
                }
            });
        }
    }

    render() {
        return (
            <>
                <FlatList
                    data={this.options}
                    renderItem={({ item }) => (
                        <ListItem onPress={() => {
                            item.func(item.screen);
                        }}
                            title={item.name}
                            titleStyle={{}}
                            subtitleStyle={{ color: "#4F8EF7" }}
                            bottomDivider
                            leftIcon={<Icon name={item.icon} size={25} color={"#4F8EF7"} />}
                        />
                    )}

                />
            </>
        );
    }
}