import React from 'react';
import { NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native';
import { ListItem } from "react-native-elements";
import Icon from 'react-native-vector-icons/Fontisto';

export default class ExistingScreen extends React.Component {
    constructor(props) {
        super(props);
        this.options = [
            { id: '1', name: "Zaufane numery telefonów", icon: "phone", screen: "PhonesScreen", func: (screen) => { this.navigate(screen) } },
            { id: '2', name: "Ustawienia zegara", icon: "clock", screen: "ClockScreen", func: (screen) => { this.navigate(screen) } },
        ];
    }

    static navigationOptions = ({ navigation }) => ({
        title: 'Ustaw istniejącą konfigurację',
    });

    navigate(screen) {
        let navigationAction = NavigationActions.navigate({
            routeName: screen,
            params: {sms: false}
        });

        this.props.navigation.dispatch(navigationAction);
    }

    render() {
        return (
            <>
                <FlatList
                    data={this.options}
                    renderItem={({ item }) => (
                        <ListItem onPress={() => {
                            item.func(item.screen);
                        }}
                            title={item.name}
                            titleStyle={{}}
                            subtitleStyle={{ color: "#4F8EF7" }}
                            bottomDivider
                            leftIcon={<Icon name={item.icon} size={25} color={"#4F8EF7"} />}
                        />
                    )}

                />
            </>
        );
    }
}