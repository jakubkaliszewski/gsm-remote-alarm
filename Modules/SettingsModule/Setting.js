export default class Setting{
    constructor(key, name, value, iconName){
        this.key = key;
        this.name = name;
        this.value = value;
        this.iconName = iconName;
    }

    toJson(){
        let json = JSON.stringify(this);
        return json;
    }

    static parseFromJson(json){
        let obj = JSON.parse(json);
        return new Setting(obj.key, obj.name, obj.value, obj.iconName);
    }
}

//TODO utworzenie na start kluczy ustawień...