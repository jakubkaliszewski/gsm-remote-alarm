import 'react-native-gesture-handler';
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import MyButton from '../../MyButton';
import Icon from 'react-native-vector-icons/Fontisto';
import InitialModal from '../InitialModule/InitialModal';
import SmsOperations from '../../SMS/SmsOperations';
import SMSStorage from '../../Database/SMSStorage';
import { scale } from 'react-native-size-matters';

export default class HomeScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            status: false,
            loading: false,
            statusText: 'Rozbrojony',
            statusIcon: 'unlocked',
            statusDate: ''
        };
    }

    async componentDidMount() {
        this.getLastStatus();
    }

    async componentDidUpdate() {
        this.getLastStatus();
    }

    async getLastStatus(){
        SMSStorage.getAlarmLastStatus().then(result => {
            if(result === null)
                this.setState({ status: null, statusText: 'Nieznany', statusIcon: 'question', statusDate: ''});
            else{
                if (result.status)
                    this.setState({ status: result.status, statusText: 'Uzbrojony', statusIcon: 'locked', statusDate: result.getDateAsString() });
                else
                    this.setState({ status: result.status, statusText: 'Rozbrojony', statusIcon: 'unlocked', statusDate: result.getDateAsString() });
            }
        });
    }

    render() {
        return (
            <>
                <InitialModal/>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ flex: 2 }}>
                        <TouchableOpacity style={{ flex: 1 }} onLongPress={() => SmsOperations.sendRefreshStatusRequest()}>
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: scale(50) }}>{this.state.statusText}<Icon style={{ padding: '200%' }} name={this.state.statusIcon} size={40}></Icon></Text>
                                <Text style={{ fontSize: scale(13), color: '#3f3f3f' }}>Status ({this.state.statusDate})</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1 }}>
                        <MyButton title="Aktywuj" color="#56ff30" onPress={() => SmsOperations.sendArmingRequest()} />
                    </View>
                    <View style={{ flex: 1 }}>
                        <MyButton title="Dezaktywuj" color="#ff3a3a" onPress={() => SmsOperations.sendDisarmingRequest()} />
                    </View>
                </View>
            </>
        );
    }
}