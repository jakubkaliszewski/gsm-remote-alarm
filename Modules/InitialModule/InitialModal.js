import React from 'react';
import { View, Text, Modal, TextInput, KeyboardAvoidingView, ToastAndroid } from 'react-native';
import MyButton from '../../MyButton';
import SMSStorage from '../../Database/SMSStorage';
import { ScaledSheet } from 'react-native-size-matters';

export default class InitialModal extends React.Component {
    constructor() {
        super();
        this.state = {
            modalVisible: null,
            phoneNumber: '',
            pinCode: '',
        };

        this.pinCodeIsValid = false;
        this.phoneNumberIsValid = false;
    }

    async componentDidMount() {
        const status = await SMSStorage.isBaselineConfigurationExist();
        this.setState({ modalVisible: !status });
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    onChangedPhoneNumber(text) {
        this.setState({ phoneNumber: text });
    }

    onChangedPin(text) {
        this.setState({ pinCode: text });
    }

    async validateForm() {
        this.validatePhoneNumber();
        this.validatePIN();

        if (this.phoneNumberIsValid && this.pinCodeIsValid) {
            await this.saveValidatedData();
            this.setModalVisible(false);
            return;
        }

        ToastAndroid.show("Wprowadź poprawne dane!", ToastAndroid.LONG);
    }

    validateNumber(text) {
        let newText = '';
        let numbers = '0123456789+';

        for (let i = 0; i < text.length; i++) {
            if (numbers.indexOf(text[i]) > -1) {
                newText = newText + text[i];
            }
            else {
                return false;
            }
        }

        return true;
    }

    validatePhoneNumber() {
        let status = this.validateNumber(this.state.phoneNumber);

        if (this.state.phoneNumber.startsWith('+') && status && this.state.phoneNumber.length == 12)
            this.phoneNumberIsValid = true;
        else {
            this.setState({ phoneNumber: '' });
            this.phoneNumberIsValid = false;
        }
    }

    validatePIN() {
        let status = this.validateNumber(this.state.pinCode);

        if (status && this.state.pinCode.length == 4)
            this.pinCodeIsValid = true;
        else {
            this.setState({ pinCode: '' });
            this.pinCodeIsValid = false;
        }
    }

    async saveValidatedData() {
        await SMSStorage.setAlarmPhoneNumber(this.state.phoneNumber);
        await SMSStorage.setAlarmPinCode(this.state.pinCode);
    }

    render() {
        return (
            <>
                <Modal
                    style={{ marginBottom: 0, padding: 0 }}
                    animationType='fade'
                    transparent={true}
                    visible={this.state.modalVisible}
                >
                    <KeyboardAvoidingView style={styles.modal} behavior="height" enabled>
                        <View style={{ flex: 1, margin: 0, padding: 0 }}>
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: '5%' }}>
                                <Text style={styles.modalTitle}>Wstępna konfiguracja</Text>
                            </View>
                            <View style={{ flex: 3, justifyContent: 'center', alignItems: 'stretch', alignSelf: 'stretch', alignContent: 'stretch', margin: 0 }}>
                                <Text style={{ marginVertical: '2%' }}>Numer alarmu: </Text>
                                <TextInput
                                    placeholder='+48123456789'
                                    placeholderTextColor={this.placeholderColor}
                                    keyboardType='phone-pad'
                                    onChangeText={(text) => this.onChangedPhoneNumber(text)}
                                    value={this.state.phoneNumber}
                                    maxLength={12}
                                    style={styles.textInput}
                                />
                                <Text style={{ marginVertical: '2%' }}>Numer PIN: </Text><TextInput
                                    placeholder='1234'
                                    placeholderTextColor={placeholderColor}
                                    keyboardType='phone-pad'
                                    onChangeText={(text) => this.onChangedPin(text)}
                                    value={this.state.pinCode}
                                    maxLength={4}
                                    style={styles.textInput}
                                />
                            </View>
                            <View style={{ flex: 2, marginTop: '5%' }}>
                                <MyButton title='Zatwierdź' color={iconColor} onPress={() => { this.validateForm() }} />
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </Modal>
            </>


        );
    }
}

const iconColor = "#4F8EF7";
const placeholderColor = "#b8b8b8";

const styles = ScaledSheet.create({
    modal: {
        marginBottom: '0%',
        marginTop: '20%',
        marginHorizontal: '5%',
        flex: 0.8,
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.46,
        shadowRadius: 9.51,
        elevation: 15,
    },

    textInput: {
        borderStyle: "solid",
        borderColor: iconColor,
        borderWidth: 1,
        borderRadius: 5,
        textAlign: 'center',
        fontSize: "20@s"
    },

    modalTitle: {
        flex: 1,
        fontWeight: 'bold',
        marginVertical: '1%',
        fontSize: "30@s"
    },
});