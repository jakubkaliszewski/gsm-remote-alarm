import moment from 'moment';
import 'moment/locale/pl';

export default class Activity {
    constructor(content, date) {
        this.date = date;
        this.content = content;
    }

    getDateAsString() {
        return moment(this.date).locale('pl').format('llll');
    }

    static generateKey(){
        let uniqueId = Math.random().toString(36).substring(2) + Date.now().toString(36);
        return '@' + uniqueId;
    }

    static compare(first, second) {
        if (first.date > second.date) return -1;
        else if (first.date < second.date) return 1;
        else return 0;
    }

    toJson(){
        let json = JSON.stringify(this);
        return json;
    }

    static parseFromJson(json){
        let obj = JSON.parse(json);
        return new Activity(obj.content, obj.date);
    }
}