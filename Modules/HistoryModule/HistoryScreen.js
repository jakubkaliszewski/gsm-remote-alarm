import React from 'react';
import { FlatList, ActivityIndicator } from 'react-native';
import { ListItem} from "react-native-elements";
import ActivityStorage from '../../Database/ActivityStorage';
import Icon from 'react-native-vector-icons/Fontisto';
import Activity from './Activity';

export default class HistoryScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activities: [],
            loading: true
        }
    }

    async componentDidMount() {
        await this.refreshActivities();
    }

    async componentDidUpdate(){
        await this.refreshActivities();
    }

    async refreshActivities(){
        await ActivityStorage.getAllActivities().then(result => {
            result.sort(Activity.compare);
            this.setState({ activities: result, loading: false })
        });
    }

    static navigationOptions = () => ({
        title: 'Historia aktywności'
    });

    render() {
        if (!this.state.loading) {
            return (
                <FlatList
                    data={this.state.activities}
                    renderItem={({ item }) => (
                        <ListItem
                            title={item.content}
                            subtitle={item.getDateAsString()}
                            titleStyle={{ }}
                            subtitleStyle={{ color: "#4F8EF7" }}
                            bottomDivider
                            rightIcon={<Icon name="info" size={25} color={"#c2c2c2"}/>}
                        />
                    )}
                    
                />
            );
        } else {
            return <ActivityIndicator />
        }
    }
}