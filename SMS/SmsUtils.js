import SendSMS from 'react-native-sms-x';
import SmsListener from 'react-native-android-sms-listener'
import { PermissionsAndroid, ToastAndroid } from 'react-native';
import Message from './Message';
import SmsOperations from './SmsOperations';

export default class SmsUtils {
    static idCounter = 0;
    static listener = null;
    static permissions = [PermissionsAndroid.PERMISSIONS.SEND_SMS,
    PermissionsAndroid.PERMISSIONS.READ_SMS,
    PermissionsAndroid.PERMISSIONS.RECEIVE_SMS,
    ];

    static async checkPermissions() {
        this.asyncForEach(this.permissions, async (permission) => {
            try {
                const granted = await PermissionsAndroid.request(permission);
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log(permission + ": Permission Granted.");
                }
                else {
                    console.log(permission + ": Permission Not Granted");
                }
            } catch (err) {
                console.warn(err);
            }
        });
    }

    static sendSms(phoneNumber, message) {
        SendSMS.send(this.idCounter, phoneNumber, message, (msg) => {
            console.log(msg);
            if (msg === 0) {
                ToastAndroid.show("Wysłano wiadomość", ToastAndroid.LONG);
                console.log("Message sent!");
            } else {
                ToastAndroid.show("Nie wysłano wiadomości!!", ToastAndroid.LONG);
                console.log("No message was sent!");
            }
        });
    }

    //TODO do sprawdzenia, by brał pod uwagę sms tylko od numeru gsm alarmu, nie innych
    static async addListener(phoneNumber, func) {
        if (this.listener == null)
            this.listener = SmsListener.addListener(msg => { 
                if(msg.originatingAddress == phoneNumber){
                    this.removeListener();
                    SmsOperations.onSMSResponse(new Message(msg.body, msg.originatingAddress), func);
                }
            });
    }

    static removeListener() {
        if (this.listener !== null) {
            this.listener.remove();
            this.listener = null;
        }
    }

    static async asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }
}