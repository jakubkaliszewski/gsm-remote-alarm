import SmsUtils from './SmsUtils';
import SmsStorage from '../Database/SMSStorage';
import OPERATION from './OperationsEnum';
import ActivityStorage from '../Database/ActivityStorage';
import Status from './Status';

export default class SmsOperations {

    //przetestowane
    static async sendRefreshStatusRequest() {
        let pinCode = await SmsStorage.getAlarmPinCode();
        let alarmPhoneNumber = await SmsStorage.getAlarmPhoneNumber();
        let smsBody = pinCode + OPERATION.STATUS;

        SmsUtils.sendSms(alarmPhoneNumber, smsBody);
        ActivityStorage.saveActivity("Wysłano żądanie odświeżenia statusu alarmu.");
        SmsUtils.addListener(alarmPhoneNumber, (response) => {
            let split = response.body.split('\n');
            if (split[2].startsWith('System ')) {
                let statusLine = split[2];
                let statusString = statusLine.split(' ')[1];
                if (statusString === 'armed.') {
                    SmsStorage.setAlarmLastStatus(new Status('true', new Date()));
                } else SmsStorage.setAlarmLastStatus(new Status('true', new Date()));
            }
        });
    }

    static async onSMSResponse(response, func) {
        if (response !== null) {
            func(response);
        }
    }

    //przetestowane
    static async sendArmingRequest() {
        let pinCode = await SmsStorage.getAlarmPinCode();
        let alarmPhoneNumber = await SmsStorage.getAlarmPhoneNumber();
        let smsBody = pinCode + OPERATION.ARMING;

        SmsUtils.sendSms(alarmPhoneNumber, smsBody);
        ActivityStorage.saveActivity("Wysłano żądanie aktywacji alarmu.");
        SmsStorage.setAlarmLastStatus(new Status(true, new Date()));
    }

    //przetestowane
    static async sendDisarmingRequest() {
        let pinCode = await SmsStorage.getAlarmPinCode();
        let alarmPhoneNumber = await SmsStorage.getAlarmPhoneNumber();
        let smsBody = pinCode + OPERATION.DISARMING;

        SmsUtils.sendSms(alarmPhoneNumber, smsBody);
        ActivityStorage.saveActivity("Wysłano żądanie dezaktywacji alarmu.");
        SmsStorage.setAlarmLastStatus(new Status(false, new Date()));
    }

    static async sendAddPhoneNumber(id, phoneNumber) {
        //PIN#Pozycja(1 - 6)Numer telefonu# -> Dodanie
        let pinCode = await SmsStorage.getAlarmPinCode();
        let alarmPhoneNumber = await SmsStorage.getAlarmPhoneNumber();
        let smsBody = pinCode + '#' + id + phoneNumber + '#';

        SmsUtils.sendSms(alarmPhoneNumber, smsBody);
        ActivityStorage.saveActivity("Wysłano żądanie dodania numeru telefonu.");
    }

    static async sendRemovePhoneNumber(idPhoneNumber) {
        //PIN#Pozycja(1 - 6)Numer telefonu# -> Dodanie
        let pinCode = await SmsStorage.getAlarmPinCode();
        let alarmPhoneNumber = await SmsStorage.getAlarmPhoneNumber();
        let smsBody = pinCode + '#' + id + '#';

        SmsUtils.sendSms(alarmPhoneNumber, smsBody);
        ActivityStorage.saveActivity("Wysłano żądanie usunięcia numeru telefonu.");
    }

    //przetestowane
    static async sendSetClock(hour, minutes) {
        //pin*2HHMM (HH - GODZINA, MM - MINUTY)
        minutesString = minutes.toString();
        hourString = hour.toString();
        if (minutesString.length === 1) {
            minutesString = '0' + minutes;
        }
        if (hourString.length === 1) {
            hourString = '0' + hour;
        }
        let pinCode = await SmsStorage.getAlarmPinCode();
        let alarmPhoneNumber = await SmsStorage.getAlarmPhoneNumber();
        let smsBody = pinCode + '*2' + hour + minutes + '*';

        SmsUtils.sendSms(alarmPhoneNumber, smsBody);
        ActivityStorage.saveActivity("Wysłano żądanie ustawienia godziny na " + hourString + ':' + minutesString);
    }

    //Czasowe uzbrajanie
    static async sendArmingClock(hourString, minutesString) {
        //pin*3HHMMT* (HH - GODZINA, MM - MINUTY, T - cyfry 1-5 okresowo)
        //T - 1 domyślnie ustawiane, na sztywno

        let pinCode = await SmsStorage.getAlarmPinCode();
        let alarmPhoneNumber = await SmsStorage.getAlarmPhoneNumber();
        let smsBody = pinCode + '*3' + hourString + minutesString + '1*';

        SmsUtils.sendSms(alarmPhoneNumber, smsBody);
        ActivityStorage.saveActivity("Wysłano żądanie czasowego uzbrojenia na " + hourString + ':' + minutesString + ". Akcja aktywna do odwołania!");
    }

    //Czasowe rozbrajanie
    static async sendDisarmingClock(hourString, minutesString) {
        //pin*4HHMMT (HH - GODZINA, MM - MINUTY, T - cyfry 1-5 okresowo)
        //T - 1 domyślnie ustawiane, na sztywno
        //T - 0 oznacza usunięcie ustawień - osobno do dodania.

        let pinCode = await SmsStorage.getAlarmPinCode();
        let alarmPhoneNumber = await SmsStorage.getAlarmPhoneNumber();
        let smsBody = pinCode + '*4' + hourString + minutesString + '1*';

        SmsUtils.sendSms(alarmPhoneNumber, smsBody);
        ActivityStorage.saveActivity("Wysłano żądanie czasowego rozbrojenia na " + hourString + ':' + minutesString + ". Akcja aktywna do odwołania!");
    }

    //Czasowe uzbrajanie Wyłączenie
    static async sendArmingClockOff() {
        //pin*3HHMMT* (HH - GODZINA, MM - MINUTY, T - cyfry 1-5 okresowo)
        //T - 1 domyślnie ustawiane, na sztywno
        let pinCode = await SmsStorage.getAlarmPinCode();
        let alarmPhoneNumber = await SmsStorage.getAlarmPhoneNumber();
        let smsBody = pinCode + '*300000*';

        SmsUtils.sendSms(alarmPhoneNumber, smsBody);
        ActivityStorage.saveActivity("Wysłano żądanie wyłączenia czasowego uzbrojenia!");
    }

    //Czasowe rozbrajanie Wyłączenie
    static async sendDisarmingClockOff() {
        //pin*4HHMMT* (HH - GODZINA, MM - MINUTY, T - cyfry 1-5 okresowo)
        //T - 1 domyślnie ustawiane, na sztywno
        //T - 0 oznacza usunięcie ustawień - osobno do dodania.

        let pinCode = await SmsStorage.getAlarmPinCode();
        let alarmPhoneNumber = await SmsStorage.getAlarmPhoneNumber();
        let smsBody = pinCode + '*400000*';

        SmsUtils.sendSms(alarmPhoneNumber, smsBody);
        ActivityStorage.saveActivity("Wysłano żądanie wyłączenia czasowego rozbrojenia!");
    }

    //Ustawienie czasowego opóźnienia w uzbrajaniu
    static async sendArmingDelay(seconds) {
        //pin*6ABC* 
        /**
         *‘A’ oznacza ilość połączeń do gospodarza od 0 do 9, 0 oznacza że
            nie przypisano żadnego pilota.
            ‘B’ oznacza opóźnienie w uzbrajaniu od 0 do 9, każda wartość jest równa 10s.
            ‘C’ oznacza opóźnienie alarmu od 0 do 9, gdzie każda wartość
            jest równa 10s.
         *
         */

        let pinCode = await SmsStorage.getAlarmPinCode();
        let alarmPhoneNumber = await SmsStorage.getAlarmPhoneNumber();
        let smsBody = pinCode + '*65' + seconds + '0*';

        SmsUtils.sendSms(alarmPhoneNumber, smsBody);
        ActivityStorage.saveActivity("Wysłano żądanie ustawienia opóźnienia uzbrojenia na " + 10 * seconds + "sekund.");
    }

    //Wyłączenie czasowego opóźnienia w uzbrajaniu
    static async sendArmingDelayOff() {
        //pin*6ABC* 
        /**
         *‘A’ oznacza ilość połączeń do gospodarza od 0 do 9, 0 oznacza że
            nie przypisano żadnego pilota.
            ‘B’ oznacza opóźnienie w uzbrajaniu od 0 do 9, każda wartość jest równa 10s.
            ‘C’ oznacza opóźnienie alarmu od 0 do 9, gdzie każda wartość
            jest równa 10s.
         *
         */

        let pinCode = await SmsStorage.getAlarmPinCode();
        let alarmPhoneNumber = await SmsStorage.getAlarmPhoneNumber();
        let smsBody = pinCode + '*60' + seconds + '0*';

        SmsUtils.sendSms(alarmPhoneNumber, smsBody);
        ActivityStorage.saveActivity("Wysłano żądanie ustawienia opóźnienia uzbrojenia na 0 sekund.");
    }
}