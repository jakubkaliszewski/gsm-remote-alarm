export default OPERATION = Object.freeze({
    STATUS: 'STATUS',
    ARMING: 'SF',
    DISARMING: 'CF'
});