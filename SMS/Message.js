export default class Message{
    constructor(body, sender){
        this.body = body;
        this.sender = sender;
    }
}