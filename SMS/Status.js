import moment from 'moment';
import 'moment/locale/pl';

export default class Status{
    constructor(status, date){
        this.status = status;
        this.date = date;
    }

    toJson() {
        let json = JSON.stringify(this);
        return json;
    }

    getDateAsString() {
        return moment(this.date).locale('pl').format('llll');
    }

    static parseFromJson(json) {
        let obj = JSON.parse(json);
        return new Status(obj.status, obj.date);
    }
}