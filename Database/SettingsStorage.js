import AsyncStorage from '@react-native-community/async-storage';

export default class SettingsStorage{

    static async getPhoneNumbers(){
        try {
            let phoneNumbers = await AsyncStorage.getItem('phoneNumbers');
            if (phoneNumbers !== null) {
                let obj = JSON.parse(phoneNumbers);
                return obj;
            }else{
                return [
                    {id: 0, phone: ''},
                    {id: 1, phone: ''},
                    {id: 2, phone: ''},
                    {id: 3, phone: ''},
                    {id: 4, phone: ''},
                    {id: 5, phone: ''},
                ]
            }
        } catch (error) {
            console.log('Phone numbers was not get!' + e);
        }
    }

    static async setPhoneNumbers(phoneNumbers){
        let stringify = JSON.stringify(phoneNumbers);
        await AsyncStorage.setItem('phoneNumbers', stringify);
    }

    static async setChangedAlarmStatus(){
        await AsyncStorage.setItem('changedAlarmStatus' , 'true');
    }

    static async unsetChangedAlarmStatus() {
        await AsyncStorage.setItem('changedAlarmStatus', 'false');
    }

    static async isChangedAlarmStatus() {
        try{
            let status = await AsyncStorage.getItem('changedAlarmStatus');
            if (status === null || status === 'false') {
                return false;
            }else{
                return true;
            }

        }catch(error){
            console.log('ChangedAlarmStatus was not get!' + e);
        }
        await AsyncStorage.setItem('changedAlarmStatus', 'true');
    }

    static async setArmingRule(timeString) {
        await AsyncStorage.setItem('armingRule', timeString);
    }

    static async setDisarmingRule(timeString) {
        await AsyncStorage.setItem('disarmingRule', timeString);
    }

    static async removeArmingRule() {
        this.setArmingRule('');
    }

    static async removeDisarmingRule() {
        this.setDisarmingRule('');
    }

    static async getArmingRule(){
        try {
            let ruleTime = await AsyncStorage.getItem('armingRule');
            if (ruleTime === null) {
                return null;
            } else {
                return ruleTime;
            }

        } catch (error) {
            console.log('ArmingRule was not get!' + e);
        }
    }

    static async getDisarmingRule() {
        try {
            let ruleTime = await AsyncStorage.getItem('disarmingRule');
            if (ruleTime === null) {
                return null;
            } else {
                return ruleTime;
            }

        } catch (error) {
            console.log('DisarmingRule was not get!' + e);
        }
    }

    static async setArmingDelayRule(seconds) {
        if (seconds === 0) {
            await AsyncStorage.setItem('armingDelayRule', '');
        }else
            await AsyncStorage.setItem('armingDelayRule', seconds);
    }

    static async getArmingDelayRule() {
        try {
            let rule = await AsyncStorage.getItem('armingDelayRule');
            if (rule === null) {
                return null;
            } else {
                return rule;
            }

        } catch (error) {
            console.log('ArmingDelayRule was not get!' + e);
        }
    }

    static async removeArmingDelayRule() {
        this.setArmingDelayRule(0);
    }

        /*
        UWAGA! WSZYSTKIE USTAWIENIA NA ROZBROJENIU ALARMU TRZEBA WYKONYWAĆ!!! -> GDY UZBROJONY DAĆ ALERT CZY KONTYNUOWAĆ (ROZBROJENIE)

        Ustawione numery telefonów => Dodaj(plus na dole listy)/usuń(długi onPress na danej pozycji...)
        PIN#Pozycja(1-6)Numer telefonu# ->Dodanie
        PIN#Pozycja(1-6)# -> Usunięcie

        Ustawienie zegara
        pin*2HHMM (HH - GODZINA, MM - MINUTY)

        Czasowe uzbrajanie

        Czasowe rozbrajanie

        Ustawienie opóźnienia uzbrajania, opóźnienia alarmu, ilości telefonów do gospodarza


        Zrobić na sztywno ekrana główny ustawień, każda z pozycji ma przekierowanie na własny ekran i tam są pobierane szczegóły tego ustawienia
        */
}