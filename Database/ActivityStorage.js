import AsyncStorage from '@react-native-community/async-storage';
import Activity from '../Modules/HistoryModule/Activity';
import { ToastAndroid } from 'react-native';


export default class ActivityStorage {

    static async saveActivity(activityContent) {
        let key = Activity.generateKey();
        activity = new Activity(activityContent, new Date());

        AsyncStorage.setItem(key, activity.toJson());
        ToastAndroid.show(activityContent, ToastAndroid.LONG);
    }

    static async getAllActivities() {
        try {
            let activitiesKeys = await AsyncStorage.getAllKeys().then(results => {
                return results.filter(record => record.startsWith('@'));//sprawdzic klucze
            });

            let activities = [];
            for(let i = 0; i < activitiesKeys.length; i++){
                activities.push(await this.getActivity(activitiesKeys[i]));
            }

            return activities;
        } catch (e) {
            console.log('The activities was not get!' + e);
        }
        return [];
    }

    static async getActivity(key) {
        try {
            const activityItem = await AsyncStorage.getItem(key);
            if (activityItem !== null) {
                let activity = Activity.parseFromJson(activityItem);
                return activity;
            }
        } catch (e) {
            console.log('The activity was not get!');
        }
    }
}