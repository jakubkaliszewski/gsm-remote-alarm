import AsyncStorage from '@react-native-community/async-storage';
import ActivityStorage from './ActivityStorage';
import Status from '../SMS/Status';

export default class SMSStorage{

    static async setAlarmPhoneNumber(phoneNumber){
        try {
            AsyncStorage.setItem('phoneNumber', phoneNumber);
            ActivityStorage.saveActivity("Ustawiono numer telefonu.");
        } catch (e) {
            console.log('The number was not set!');
        }
    }

    static async getAlarmPhoneNumber(){
        try {
            const number = await AsyncStorage.getItem('phoneNumber')
            if (number !== null) {
                return number;
            }
        } catch (e) {
            console.log('The number was not get!');
        }
    }

    static async setAlarmPinCode(pinCode) {
        try {
            AsyncStorage.setItem('pinCode', pinCode);
            ActivityStorage.saveActivity("Ustawiono numer pin.");
        } catch (e) {
            console.log('The pincode was not set!');
        }
    }

    static async getAlarmPinCode() {
        try {
            const pinCode = await AsyncStorage.getItem('pinCode')
            if (pinCode !== null) {
                return pinCode;
            }
        } catch (e) {
            console.log('The pincode was not get!');
        }
    }

    static async isAlarmPhoneNumberExist(){
        try {
            const number = await AsyncStorage.getItem('phoneNumber')
            if (number != null) {
                return true;
            }else{
                return false;
            }
        } catch (e) {
            console.log('The number was not get!');
        }
    }

    static async isAlarmPinCodeExist() {
        try {
            const pinCode = await AsyncStorage.getItem('pinCode')
            console.log('PIN: ' + pinCode);
            if (pinCode != null) {
                return true;
            } else {
                return false;
            }
        } catch (e) {
            console.log('The pincode was not get!');
        }
    }

    static async isBaselineConfigurationExist(){
        if(await this.isAlarmPhoneNumberExist() && await this.isAlarmPinCodeExist()){
            return true;
        }
        return false;
    }

    static async setAlarmLastStatus(status){
        AsyncStorage.setItem('lastStatus', status.toJson());
    }

    static async getAlarmLastStatus(){
        try {
            const status = await AsyncStorage.getItem('lastStatus');
            if (status == null) {
                return null;
            }
            return Status.parseFromJson(status);
        } catch (e) {
            console.log('The lastStatus was not get!');
            return null;
        }
    }
}