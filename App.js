import 'react-native-gesture-handler'
import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import Icon from 'react-native-vector-icons/Fontisto';

import HistoryScreen from './Modules/HistoryModule/HistoryScreen';
import HistoryDetails from './Modules/HistoryModule/HistoryScreen';
import HomeScreen from './Modules/HomeModule/HomeScreen';
import SettingsNavigator from './Modules/SettingsModule/SettingsNavigator';
import SmsUtils from './SMS/SmsUtils';

const iconColor = "#4F8EF7";

const HistoryNavigator = createStackNavigator({
  History: {
    screen: HistoryScreen
  },
  Details: {
    screen: HistoryDetails
  }
}, {
    initialRouteName: 'History',
    defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: 'white',
    },
    headerTintColor: 'black',
    headerTitleStyle: {
      fontWeight: 'bold',
      textAlign: 'center',
      flex: 1,
    },
  },
});   

const AppNavigator = createBottomTabNavigator({
  Settings: {
    screen: SettingsNavigator,
    navigationOptions: {
      tabBarIcon: () => (<Icon name="player-settings" size={25} color={iconColor} />),
      title: 'Ustawienia'
    }
  },
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      tabBarIcon: () => (<Icon name="home" size={25} color={iconColor} />)
    }
  },

  History: {
    screen: HistoryNavigator,
    navigationOptions: {
      tabBarIcon: () => (<Icon name="nav-icon-list-a" size={20} color={iconColor} />),
      title: 'Historia'
    }
  },
},{
  initialRouteName: 'Home'
}
);


const AppContainer = createAppContainer(AppNavigator);
class App extends React.Component {

  async componentDidMount() {
    await SmsUtils.checkPermissions();
  }
  
  render() {
    return (
      <AppContainer />
    );
  }
}
export default App;