# GSM Remote Alarm

Aplikacja służy do prostej i czytelnej obsługi alarmu sterowanego przy pomocy SMS (alarm GSM).
Alarm stanowi urządzenie „System alarmowy RED I” – [strona dystrybutora na terenie Polski](http://pomoc.big5.pl/faq/content/66/System-alarmowy-RED-I).

## Funkcjonalności

### Konfiguracja wstępna

Przy pierwszym uruchomieniu ukazuje się modal zawierający prosty formularz, w którym podajemy:
- numer telefonu alarmu GSM,
- numer pin, ustawiony na alarmie.

Powyższe dane są niezbędne do korzystania z alarmu w trybie GSM. Ponadto przy każdorazowym wysłaniu wiadomości SMS na alarm widoczny jest Toast mówiący o wysyłaniu SMSa.

### Ekran główny (Home)

Widoczny jest ostatni pozyskany przez aplikację status alarmu. Wyświetlana jest dokładna data wraz z godziną. Po dłuższym przyciśnięciu nastąpi wysłanie SMSa w celu uzyskania aktualnego statusu od alarmu GSM. Przycisk Aktywuj, normalne naciśnięcie przycisku powoduje wysłanie żądania aktywacji alarmu. Przycisk Dezaktywuj, powoduje dezaktywację alarmu.

### Ekran Ustawień

Dla każdego ustawienia bieżące ustawienie wyświetlane jest na niebiesko pod tytułem opcji. Ponadto przed wejściem i opuszczeniem ekranów opcji sprawdzany jest ostatni znany status alarmu z tym, który powinien być. Dzieje się tak dlatego, że alarm konfigurowany może być tylko stanie „Dezaktywowany” i niepożądane jest nagłe zdjęcie blokady przez zastosowanie ustawień z „domu”.

- Zaufane numery telefonów: podajemy numery telefonów, które mają zostać powiadomione w przypadku wywołania alarmu – dzwonienie na wskazany numer.

- Ustawienia zegara:
  - Ustaw zegar alarmu – ustawiamy obecny czas na alarmie;
  - Czasowe uzbrajanie – ustawiamy regułę automatycznej aktywacji alarmu o wskazanej godzinie;
  - Czasowe rozbrajanie – ustawiamy regułę automatycznej dezaktywacji alarmu o wskazanej godzinie;
  - Opóźnienie uzbrojenia – aktywacja alarmu następuje po X sekundach po otrzymaniu żądania aktywacji (na przykład dajemy sobie 30 sekund na opuszczenie budynku);
  - Opcje usuń - usuwają wskazaną regułę.

- Ustaw istniejącą konfigurację: polega na ustawieniu wszystkich opisanych wyżej opcji bez wysyłania żądań SMS do alarmu.

### Ekran Historia

Służy do logowania aktywności użytkownika. Można posłużyć się tym w celu sprawdzenia, kiedy ostatnio coś robiłem itp., bądź upewnić się, że dana reguła została wysłana.

## Galeria

![2019-20-JK-GSMRemoteAlarm-06](https://gitlab.com/jakubkaliszewski/gsm-remote-alarm/-/wikis/uploads/a19078230e2eb54fb63674609894d02e/2019-20-JK-GSMRemoteAlarm-06.jpg)
![2019-20-JK-GSMRemoteAlarm-05](https://gitlab.com/jakubkaliszewski/gsm-remote-alarm/-/wikis/uploads/4560bdbed766997fc34063fc96b620f4/2019-20-JK-GSMRemoteAlarm-05.jpg)
![2019-20-JK-GSMRemoteAlarm-04](https://gitlab.com/jakubkaliszewski/gsm-remote-alarm/-/wikis/uploads/6226bdfef8a358176dc7921c0bf2b2e7/2019-20-JK-GSMRemoteAlarm-04.jpg)

![2019-20-JK-GSMRemoteAlarm-03](https://gitlab.com/jakubkaliszewski/gsm-remote-alarm/-/wikis/uploads/88856d548de466fe1ea63cc85c2b10b5/2019-20-JK-GSMRemoteAlarm-03.jpg)
![2019-20-JK-GSMRemoteAlarm-02](https://gitlab.com/jakubkaliszewski/gsm-remote-alarm/-/wikis/uploads/305db1306a848cafc80ec0a3bb402669/2019-20-JK-GSMRemoteAlarm-02.jpg)
![2019-20-JK-GSMRemoteAlarm-01](https://gitlab.com/jakubkaliszewski/gsm-remote-alarm/-/wikis/uploads/7b75ba3ed5faddaa397094667fb04aa8/2019-20-JK-GSMRemoteAlarm-01.jpg)
