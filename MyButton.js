import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';


export default class MyButton extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableOpacity {...this.props}>
                <View
                    style={{
                        backgroundColor: this.props.color !== undefined ? this.props.color : 'black',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 15,
                        padding: 15,
                        minWidth: this.props.minWidth !== undefined ? this.props.minWidth : '60%',
                        minHeight: '50%'
                    }}>
                    <Text style={styles.textStyle}>
                        {this.props.title}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = ScaledSheet.create({
    textStyle: {
        color: 'white', fontSize: "20@ms", fontWeight: 'bold'}
    });